﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyAI : MonoBehaviour
{
    private GameObject player;
    private SpawnManager spawnManager;
    private float speed;

    public float health;
    public float maxHealth = 100;
    public Slider healthSlider;

    void Start()
    {
        health = maxHealth;
        player = GameObject.Find("Player");
        spawnManager = GameObject.Find("SpawnManager").GetComponent<SpawnManager>();
        speed = spawnManager.enemySpeed;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += (player.transform.position - transform.position).normalized * speed * Time.deltaTime;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("GAME OVER");
        }
        if (collision.gameObject.CompareTag("Bullet"))
        {
            Destroy(collision.gameObject);
            //later damage input will be : TakeDamage(collision.gameObject.damage)
            TakeDamage(20);
        }
    }

    void TakeDamage(int damageDealt)
    {
        health -= damageDealt;
        healthSlider.value = health / maxHealth;
        if (health <= 0)
        {
            spawnManager.enemyKillCount++;
            spawnManager.textKillCount.text = "Enemies Slained : " + spawnManager.enemyKillCount;
            StartCoroutine(Death());
        }
    }

    IEnumerator Death()
    {
        yield return new WaitForSeconds(2f);
        Destroy(gameObject);
    }
}
