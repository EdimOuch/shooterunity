﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Difficulty : MonoBehaviour
{
    private Button difficultyButton;
    private SpawnManager spawnManager;

    public int difficulty;

    void Start()
    {
        difficultyButton = GetComponent<Button>();
        spawnManager = GameObject.Find("SpawnManager").GetComponent<SpawnManager>();
        difficultyButton.onClick.AddListener(SetDifficultyHigher);
    }

    void SetDifficultyHigher()
    {
        spawnManager.enemySpeed = difficulty;
        spawnManager.StartGame();
    }
}
