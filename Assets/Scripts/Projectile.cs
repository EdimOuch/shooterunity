﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float shotPower;
    public Vector3 worldPosition;

    private Rigidbody projectileRb;
    Plane plane = new Plane(new Vector3(0, 0.71f, 0), 0);
    float boundMin = -24;
    float boundMax = 24;
    void Start()
    {
        projectileRb = GetComponent<Rigidbody>();
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float distance;
        if (plane.Raycast(ray, out distance))
        {
            worldPosition = ray.GetPoint(distance);
        }
        projectileRb.AddForce((worldPosition - transform.position).normalized * shotPower, ForceMode.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x > boundMax || transform.position.x < boundMin || transform.position.z < boundMin || transform.position.z > boundMax)
        {
            Destroy(gameObject);
        }
    }

}
