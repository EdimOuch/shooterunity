﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerController : MonoBehaviour
{
    public GameObject projectilePrefab;
    public GameObject cursor;
    public GameObject gunMouth;
    public TextMeshProUGUI textBulletCount;
    public Slider slider;
    public SpawnManager spawnManager;

    private int bulletCount;
    private Rigidbody playerRb;
    private float speed = 5f;
    private float zMax = 24;
    private float zMin = -24;
    private float xMax = 24;
    private float xMin = -24;
    private Vector3 offset = new Vector3(0.5f, 0f, 0.5f);
    private bool isReloading;
    private float reloadSpeed = 2f;
    void Start()
    {
        spawnManager = GameObject.Find("SpawnManager").GetComponent<SpawnManager>();
        bulletCount = 10;
        slider.maxValue = reloadSpeed;
        playerRb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (spawnManager.isGameActive)
        {
            LookAt();
            if (isReloading)
            {
                slider.value += Time.deltaTime;
            }
            if (Input.anyKey)
                InputControl();
        }
    }

    void LookAt()
    {
        transform.LookAt(new Vector3(cursor.transform.position.x, 0.71f, cursor.transform.position.z));
    }

    void InputControl()
    {
        if (Input.GetKey(KeyCode.Z) && transform.position.z < zMax)
        {
            transform.position += Vector3.forward * Time.deltaTime * speed;
            Camera.main.transform.position += Vector3.forward * Time.deltaTime * speed;
        }
        if (Input.GetKey(KeyCode.S) && transform.position.z > zMin)
        {
            transform.position += Vector3.back * Time.deltaTime * speed;
            Camera.main.transform.position += Vector3.back * Time.deltaTime * speed;
        }
        if (Input.GetKey(KeyCode.Q) && transform.position.x > xMin)
        {
            transform.position += Vector3.left * Time.deltaTime * speed;
            Camera.main.transform.position += Vector3.left * Time.deltaTime * speed;
        }
        if (Input.GetKey(KeyCode.D) && transform.position.x < xMax)
        {
            transform.position += Vector3.right * Time.deltaTime * speed;
            Camera.main.transform.position += Vector3.right * Time.deltaTime * speed;
        }

        if (Input.GetMouseButtonDown(0))
        {
            Shoot();
        }
        if(Input.GetKeyDown(KeyCode.R) && bulletCount != 0 && bulletCount != 10)
        {
            bulletCount = 0;
            StartCoroutine(Reload());
        }

    }

    void Shoot()
    {
        if (bulletCount > 0)
        {
            Instantiate(projectilePrefab, gunMouth.transform.position, projectilePrefab.transform.rotation);
            bulletCount--;
            textBulletCount.text = "" + bulletCount;
            if (bulletCount == 0)
            {
                StartCoroutine(Reload());
            }
        }
    }

    IEnumerator Reload()
    {
        textBulletCount.text = "Reloading...";
        slider.gameObject.SetActive(true);
        isReloading = true;
        yield return new WaitForSeconds(reloadSpeed);
        isReloading = false;
        bulletCount = 10;
        textBulletCount.text = "" + bulletCount;
        slider.value = 0;
        slider.gameObject.SetActive(false);
        yield break;
    }
}
